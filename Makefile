SHELL=/bin/bash

PERF := $(shell command -v perf 2> /dev/null)

fuzz: setup_kernel
	cargo afl build --features fuzz
	cp target/debug/list_flattener target/debug/list_flattener-afl_fuzzer || true
	cargo afl fuzz -i fuzz_input/ -o fuzz_output/ target/debug/list_flattener-afl_fuzzer
setup_kernel:
	if [[ "core" != "$$(cat /proc/sys/kernel/core_pattern)" ]] ; then echo core | sudo tee /proc/sys/kernel/core_pattern; fi

run:
	cargo run

run_trace:
	RUST_BACKTRACE=all cargo run

check_perf:
ifndef PERF
	$(error "perf is not available please install perf (Arch) linux-tools (Debian)")
endif

flamegraph: check_perf
	sudo sysctl kernel.perf_event_paranoid=-1
	cargo flamegraph
	sudo sysctl kernel.perf_event_paranoid=3

flamegraph_dev: check_perf
	sudo sysctl kernel.perf_event_paranoid=-1
	cargo flamegraph --dev
	sudo sysctl kernel.perf_event_paranoid=3
