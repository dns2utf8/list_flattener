# list_flattener

[![crates.io](https://img.shields.io/crates/v/list_flattener)](https://crates.io/crates/list_flattener)
[![pipeline](https://img.shields.io/gitlab/pipeline/dns2utf8/list_flattener)](https://gitlab.com/dns2utf8/list_flattener/)

Parses and flattens lists of integer values:

`[[[[[[ 0x6 ]]], [[[[[ 0x500 ]]]]], 42_42]]]` -> `[6, 0x500, 4242]`

## Fuzzing

If you found a crash with `make fuzz` you can verify it with this command:

```bash
echo "[[42]" | make run
```

## Flamegraph

Install deps:

* `sudo apt install linux-tools-common linux-tools-generic`
* `cargo install flamegraph`


