#[cfg(feature = "fuzz")]
#[macro_use]
extern crate afl;

#[cfg(not(feature = "fuzz"))]
#[cfg_attr(tarpaulin, skip)]
fn main() -> std::io::Result<()> {
    let mut buffer = String::new();
    /* {
    use std::io::{self, Read};
    io::stdin().read_to_string(&mut buffer)?;
    } */
    buffer += "[ 42, 0x90, 0o42, [[[[[ 0x666 ]]]]] ]";
    let result = list_flattener::build_structure(&buffer);
    println!("{:#?}", result);

    match result {
        Ok(result) => {
            let flat = result.iter().collect::<Vec<_>>();
            println!("{:#?}", flat);
        }
        Err(e) => {
            println!("{:?}", e);
        }
    };

    Ok(())
}

#[cfg(feature = "fuzz")]
fn main() {
    fuzz!(|data: &[u8]| {
        if let Ok(s) = std::str::from_utf8(data) {
            let _ = list_flattener::build_structure(&s);
        }
    });
}
