use core::iter::once;
use core::str::Chars;
use parse_int::parse as parse_int;

#[derive(Debug, PartialEq)]
pub enum NestedList {
    Num(isize),
    List(Vec<NestedList>),
}
use NestedList::*;

#[derive(Debug, PartialEq)]
pub enum ParseError {
    InvalidElement,
    IncompleteInput,
    TailingInput,
    ExpectedCommaOrEnd,
}
use ParseError::*;

impl NestedList {
    pub fn iter<'a>(&'a self) -> Box<(dyn Iterator<Item = isize> + 'a)> {
        match self {
            Num(n) => Box::new(once(*n)),
            List(l) => Box::new(l.iter().flat_map(|e| e.iter())),
        }
    }
}

pub fn build_structure(input: &str) -> Result<NestedList, ParseError> {
    //println!("build_structure({:?})", input);
    let mut chars = input.trim().chars();

    let list = parse_element(&mut chars)?;

    let tail = chars.collect::<String>();
    if tail.is_empty() {
        Ok(list)
    } else {
        Err(TailingInput)
    }
}

fn parse_element(chars: &mut Chars) -> Result<NestedList, ParseError> {
    //println!("parse_element({:?})", chars);
    skip_whitespace(chars);
    if peek(chars)? == '[' {
        parse_list_elements(chars)
    } else {
        let mut buf = String::new();
        while let Ok(c) = peek(chars) {
            //println!("    parse_element::while => {}", c);
            if c == ' ' || c == ',' || c == ']' {
                break;
            }
            buf.push(c);
            chars.next();
        }
        //println!("    parse_element::buf=> {}", buf);
        if let Ok(num) = parse_int(&buf) {
            Ok(Num(num))
        } else {
            Err(InvalidElement)
        }
    }
}

/// Parse: "[" (Element ("," Element)*)? "]"
fn parse_list_elements(chars: &mut Chars) -> Result<NestedList, ParseError> {
    //println!("parse_list_elements({:?})", chars);
    skip_whitespace(chars);
    let mut l = vec![];

    if chars.next().expect("called after check") != '[' {
        unreachable!("parse_list_elements:> [");
    }

    skip_whitespace(chars);
    let mut expect_new_element = false;
    while let Ok(c) = peek(chars) {
        //println!("    parse_list_elements::for:> c = {:?}", c);
        match c {
            ']' => {
                //println!("    parse_list_elements::for:> ]");
                let _ = chars.next().expect("checked with peek (']')");
                return Ok(List(l));
            }
            ',' => {
                let _ = chars.next().expect("checked with peek (',')");
                expect_new_element = false;
            }
            _ if expect_new_element == false => {
                // next element
                let el = parse_element(chars)?;
                //println!("    parse_list_elements::for:> el = {:?}", el);
                l.push(el);
                skip_whitespace(chars);
                expect_new_element = true;
            }
            _ => {
                return Err(ExpectedCommaOrEnd);
            }
        }
    }
    Err(IncompleteInput)
}

fn skip_whitespace(chars: &mut Chars) {
    //println!("skip_whitespace({:?})", chars);
    while let Some(c) = chars.clone().next() {
        if c != ' ' {
            return;
        }
        chars.next();
    }
}

fn peek(chars: &Chars) -> Result<char, ParseError> {
    match chars.clone().next() {
        Some(c) => Ok(c),
        None => Err(IncompleteInput),
    }
}

#[cfg(test)]
mod test_parser_tools {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn do_peek() {
        let a = "abc";
        let mut c = a.chars();

        assert_eq!(Ok('a'), peek(&mut c));
        assert_eq!(Ok('a'), peek(&mut c));
        assert_eq!(Ok('a'), peek(&mut c));

        assert_eq!(a, c.collect::<String>());
    }

    #[test]
    fn incomplete_peek() {
        let a = "";
        let mut c = a.chars();

        assert_eq!(Err(IncompleteInput), peek(&mut c));
    }

    #[test]
    fn do_skip_whitespace() {
        let a = "    abc    ";
        let mut c = a.chars();

        skip_whitespace(&mut c);

        assert_eq!("abc    ", c.collect::<String>());
    }
}

#[cfg(test)]
mod test_parser {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn empty_list() {
        assert_eq!(Ok(List(vec![])), build_structure("[]"));
    }

    #[test]
    fn naked_num() {
        assert_eq!(Ok(Num(2)), build_structure("2"));
    }

    #[test]
    fn naked_hex_num() {
        assert_eq!(Ok(Num(0x24)), build_structure("0x24"));
    }

    #[test]
    fn linear_list() {
        assert_eq!(
            Ok(List(vec![Num(1), Num(2), Num(3),])),
            build_structure("[1, 2, 3]")
        );
    }

    #[test]
    fn linear_list_trim() {
        assert_eq!(
            Ok(List(vec![Num(1), Num(2), Num(3),])),
            build_structure("    [1, 2, 3]    ")
        );
    }

    #[test]
    fn increasing_nested_list() {
        assert_eq!(
            Ok(List(vec![Num(1), List(vec![Num(2), List(vec![Num(3)]),]),])),
            build_structure("[1, [2, [3]]]")
        );
    }

    #[test]
    fn decreasing_nested_list() {
        assert_eq!(
            Ok(List(vec![List(vec![List(vec![Num(3)]), Num(2),]), Num(1),])),
            build_structure("[[[3], 2], 1]")
        );
    }

    // Errors

    #[test]
    fn empty_input() {
        assert_eq!(Err(IncompleteInput), build_structure(""));
    }
    #[test]
    fn incomplete_input() {
        assert_eq!(Err(IncompleteInput), build_structure("["));
    }

    #[test]
    fn invalid_element() {
        assert_eq!(Err(InvalidElement), build_structure("asdf"));
    }

    #[test]
    fn tailing_input() {
        assert_eq!(Err(TailingInput), build_structure("[] tail"));
    }

    #[test]
    fn just_closed() {
        assert_eq!(Err(InvalidElement), build_structure("]"));
    }

    #[test]
    fn missing_comma() {
        assert_eq!(Err(ExpectedCommaOrEnd), build_structure("[ 0 1 ]"));
    }
}

#[cfg(test)]
mod test_flatter {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn single() {
        let l = Num(1);

        assert_eq!(vec![1], l.iter().collect::<Vec<_>>());
    }

    #[test]
    fn flat_flat() {
        let l = List(vec![Num(1), Num(2), Num(3)]);

        assert_eq!(vec![1, 2, 3], l.iter().collect::<Vec<_>>());
    }

    #[test]
    fn ascending() {
        let l = List(vec![Num(1), List(vec![Num(2), List(vec![Num(3)])])]);

        assert_eq!(vec![1, 2, 3], l.iter().collect::<Vec<_>>());
    }

    #[test]
    fn descending() {
        let l = List(vec![List(vec![List(vec![Num(3)]), Num(2)]), Num(1)]);

        assert_eq!(vec![3, 2, 1], l.iter().collect::<Vec<_>>());
    }
}

#[cfg(test)]
mod test_e2e {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn deep() {
        let s = build_structure("[[[[[[ 6 ]]]]]]").unwrap();
        assert_eq!(vec![6], s.iter().collect::<Vec<_>>());
    }

    #[test]
    fn deep_hex() {
        let s = build_structure("[[[[[[ 0x6 ]]], [[[[[ 0x500 ]]]]], 42_42]]]").unwrap();
        assert_eq!(vec![6, 0x500, 4242], s.iter().collect::<Vec<_>>());
    }
}
